import React, { Component } from "react";
import { Map, TileLayer, Marker, Popup } from "react-leaflet";
import { divIcon } from "leaflet";
import axios from "axios";
import "leaflet/dist/leaflet.css";
import CircularProgressbar from 'react-circular-progressbar';

const BGS = [
    "https://www.corinthia.com/application/files/6315/0460/7084/corinthia-hotel-tripoli-lobby.jpg",
    "https://www.hotel-chinzanso-tokyo.com/wp-content/uploads/sites/225/2015/11/Hotel-Chinzanso-Tokyo_spa1.jpg",
    "https://2486634c787a971a3554-d983ce57e4c84901daded0f67d5a004f.ssl.cf1.rackcdn.com/florida-hotel-and-conference-center/media/FH-Gallery-1-598b45769980a.jpg",
    "https://www.lopesan.com/img/hotels/88/fotos-Lopesan-Baobab-Resort-zona-piscina-lagos01.jpg",
    "https://www.hotel-anis.com/wp-content/uploads/2017/05/home2.jpg"
];

const HEADLINES = [
    "Supercharge your conversions with Image AI from DeepPulse",
    "Transform your listings into intelligent recommendations",
    "Bring Storytelling to your existing Image Product"
];

const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const API_SERVER = "http://62.210.93.54:3000/";

const MARKER_ICON = divIcon({className: 'home-icon',iconSize: [50, 50], popupAnchor:[0,-20]});

export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            countries:[],
            country:"",
            showModal:false,
            currentImage:"",
            clickedMenuIndex:-1,
            center:[38.586937, -121.503351],
            dataToShow:null,
            apiError:null,
            locationCardOpen:false,
            imageQualityCardOpen:false,
            imageQualityData:null,
            imageQualityCallInProgress:false,
            goodImageQualities:[],
            badImageQualities:[],
            viewTypeApicallInprogress:false,
            viewTypeCardOpen:false,
            imageScore:null,
            viewTypeCallInProgress:false,
            imageViewData:null,
            selectedImageCategoryIndex:-1,
            imageSubcategories:[],
            objectsCardOpen:false,
            objectApicallinprogress:false,
            imageObjectData:null,
            currentRectangles:[],
            showBoundingBoxes:false,
            clickedBoundingBoxIndex:-1,
            selectedBoundingBoxSubcategories:{},
            outsideApicallinprogress:false,
            outsideObjectData:null
        };

        this.topImage = null;
        this.bottomImage = null;
    }

    componentDidMount(){
        this.mainTitle.innerHTML = HEADLINES[0];
        this.enableBackgroundAnimation();
        this.enableTextAnimation();
    }

    enableTextAnimation(){
        this.textCounter = 1;
        this.textAnimationInterval = setInterval(()=>{
            this.mainTitle.style.opacity = 0;
            setTimeout(()=>{
                if(this.textCounter < HEADLINES.length - 1){
                    this.mainTitle.innerHTML = HEADLINES[this.textCounter++];
                } else {
                    this.mainTitle.innerHTML = HEADLINES[this.textCounter];
                    this.textCounter = 0;
                }
                this.mainTitle.style.opacity = 1;
            },900);
        },5000);
    }

    enableBackgroundAnimation(){
        this.counter = 1;
        this.animationInterval = setInterval(()=>{
            this.topImage.style.opacity = 0;
            this.topImage.style.transform = "scale(1.2)";
            setTimeout(()=>{
                if(this.counter < BGS.length - 1){
                    this.topImage.src = BGS[this.counter++];   
                } else {
                    this.topImage.src = BGS[this.counter];   
                    this.topImage.style.transform = "scale(1)";
                    this.counter = 0;
                }
                this.topImage.style.opacity = 1;
            },900);
        },5000);
    }

    getIscoChronesData(location){
		this.setState({
            apiError:null,
            dataToShow:null
		},()=>{
			axios({
				method:"GET",
				url:`http://62.210.93.54:8801/poi?lat=${location.lat}&lon=${location.lng}`
			}).then((response)=>{
                console.log(response);
                if(response.data.status == "error"){
                    this.setState({
                        dataToShow:null,
                        apiError:"No Data found"
                    });
                } else {
                    var dataObject = {
                        vegetation:response.data.peaks.result.vegetation,
                        beach:response.data.peaks.result.beach,
                        ocean:response.data.peaks.result.ocean,
                        river:response.data.peaks.result.river,
                        attraction:response.data.attraction
                    };
                    this.setState({
                        dataToShow:dataObject
                    });
                }
			}).catch((err)=>{
                console.log(err);
                this.setState({
                    dataToShow:null,
                    apiError:"No Data found"
                });
			})
		})
    }

    getImageQuality(){
        this.setState({
            imageQualityCallInProgress:true
        },()=>{
            axios({
                method:"GET",
                url:"http://51.15.164.118:6006/imagequality?url=" + this.state.currentImage
            }).then((response)=>{
                var goodArray = [], badArray = [];
                if(response.data["is_blurred"] == true){
                    badArray.push("Blurred")
                } else {
                    goodArray.push("Sharp")
                }

                if(response.data["is_bright"] == true){
                    goodArray.push("Bright")
                } else {
                    badArray.push("Dark")
                }

                if(response.data["is_colorful"] == true){
                    goodArray.push("Colorful")
                } else {
                    badArray.push("Dull")
                }

                if(response.data["is_inverted"] == true){
                    badArray.push("Inverted")
                } else {
                    goodArray.push("Not inverted")
                }

                if(response.data["is_pixelated"] == true){
                    badArray.push("Pixelated")
                } else {
                    goodArray.push("Not Pixelated")
                }

                if(response.data["is_straight"] == true){
                    goodArray.push("Straight")
                } else {
                    badArray.push("Tilted")
                }

                if(response.data["is_zoomIn"] == true){
                    badArray.push("Zoomed In")
                } else {
                    goodArray.push("Not Zoomed In")
                }

                if(response.data["is_zoomOut"] == true){
                    badArray.push("Zoomed Out")
                } else {
                    goodArray.push("Not Zoomed Out")
                }

                this.setState({
                    imageQualityCallInProgress:false,
                    imageQualityData : response.data,
                    badImageQualities:badArray,
                    goodImageQualities:goodArray
                });
            }).catch((err)=>{
                this.setState({
                    imageQualityCallInProgress:false,
                    imageQualityData:null,
                    badImageQualities:[],
                    goodImageQualities:[]
                })
            })
        })
    }

    getAestheticScore(){
        axios({
            method:"GET",
            url:"http://51.15.164.118:5000/getAesthetic/v3?url=" + this.state.currentImage
        }).then((response)=>{
            if(response.data.error){
                this.setState({
                    imageScore:null
                });
            } else {
                this.setState({
                    imageScore:response.data.mean
                });
            }
            
        }).catch((err)=>{
            console.log(err);
        })
    }

    getImageViewType(){
        this.setState({
            viewTypeApicallInprogress: true
        },()=>{
            axios({
                method:"GET",
                url:"http://62.210.100.210:8472/places?url=" + this.state.currentImage   
            }).then((response)=>{
                console.log(response.data);
                if(response.data.result.sceneAttributes == undefined){
                    this.setState({
                        viewTypeApicallInprogress:false,
                        imageViewData:null
                    })
                } else {
                    this.setState({
                        viewTypeApicallInprogress:false,
                        imageViewData:response.data.result
                    })
                }
                
            }).catch((err)=>{
                console.log(err);
                this.setState({
                    viewTypeApicallInprogress:false,
                    imageViewData:null
                })
            })
        })
    }

    getObjectData(){
        this.setState({
            viewTypeApicallInprogress: true
        },()=>{
            axios({
                method:"GET",
                url:"http://35.224.117.133:8094/semantic?url=" + this.state.currentImage   
            }).then((response)=>{
                var wRatio = parseFloat(this.cimage.width / 300).toFixed(3);
                var hRatio = parseFloat(this.cimage.height / 300).toFixed(3);
                var allRecs = [];

                if(Object.keys(response.data.tags.objects.bbox).length == 0){
                    this.setState({
                        imageObjectData:null
                    })
                } else {
                    Object.keys(response.data.tags.objects.bbox).map((box)=>{
                        var tempRectangles = response.data.tags.objects.bbox[box];
                        var newRecs = [];
                        tempRectangles.map((tr)=>{
                            var obj = {}
                            obj.top = tr[1] * hRatio;
                            obj.left=  tr[0] * wRatio;
                            obj.width = tr[2] * wRatio;
                            obj.height = tr[3] * hRatio;
                            obj.name = box;
                            obj.subcat = response.data.tags.objects.subcategory[box]
                            newRecs.push(obj);
                        })   
                        allRecs = allRecs.concat(newRecs);
                    });
    
                    this.setState({
                        objectApicallinprogress:false,
                        imageObjectData:response.data,
                        currentRectangles:allRecs
                    });
                }
            }).catch((err)=>{
                console.log(err.response);
                this.setState({
                    objectApicallinprogress:false,
                    imageObjectData:null
                })
            })
        })
    }

    getOutsideViewData(){
        this.setState({
            outsideApicallinprogress:true
        },()=>{
            axios({
                method:"GET",
                url:`http://35.224.117.133:8095/semantic?url=${this.state.currentImage}&lat=${this.state.center[0]}&lon=${this.state.center[1]}`
            }).then((response)=>{
                this.setState({
                    outsideApicallinprogress:false,
                    outsideObjectData:response.data.tags.objects.views.details
                })
            }).catch((err)=>{
                console.log(err);
                this.setState({
                    outsideApicallinprogress:false,
                    outsideObjectData:null
                })
            })
        })
    }
    render() {
        return (
            <div className="home-wrapper">
                <div className="img-wrap">
                    <img className="top-img imgg" src={BGS[0]} ref={node => this.topImage = node}/>
                </div>
                <section className="header" ref={node => this.headerElem = node}>
                    <div className="width-wrap">
                        <div className="clogo">
                            <img src={require("../images/support.png")} />
                        </div>
                    </div>
                </section>
                <section className="intro" style={{height:window.innerHeight}}>
                    <div className="mask"/>
                    <div className="inner-container">
                        <div style={{width:"100%"}}>
                            <div className="sec-title">
                                <div className="t" ref={node => this.mainTitle = node}>
                                    Book unique homes and experiences all over the world.
                                </div>
                            </div>
                            <div className="form-field search">
                                <input 
                                    placeholder="Try some image URL" 
                                    type="text" 
                                    className="inp" 
                                    value={this.state.currentImage}
                                    onChange={(event)=>{
                                        this.setState({
                                            currentImage:event.target.value
                                        });
                                    }} />
                                    {
                                        this.state.currentImage !== "" ?
                                            <div className="go-btn" onClick={()=>{
                                                this.setState({
                                                    showModal:true
                                                },()=>{
                                                    this.getImageViewType();
                                                    this.getImageQuality();
                                                    this.getObjectData();
                                                    this.getAestheticScore();
                                                    this.getOutsideViewData();
                                                });
                                            }}>Go!</div>
                                        :
                                        null
                                    }
                            </div> 
                        </div>  
                    </div>
                </section>
                {
                    this.state.showModal ?
                    <div className="modal-wrap">
                        <div className="modal">
                            <div className="modal-mask"/>
                            <img className="close-btn" src={require("../images/clear-button.svg")} onClick={()=>{
                                this.setState({
                                    showModal:false,
                                    imageQualityData:null,
                                    locationCardOpen:false,
                                    imageQualityCardOpen:false,
                                    viewTypeCardOpen:false,
                                    currentRectangles:[],
                                    selectedImageCategoryIndex:-1
                                });
                            }}/>
                            <div className="img-holder">
                                <img src={this.state.currentImage} className="img-hh" ref={node => this.cimage = node}/>
                                <div className="dcanvas">
                                    {
                                        this.state.showBoundingBoxes && this.state.currentRectangles.map((rec,index)=>{
                                            return(
                                                <div className={`rec` + (rec.flash ? " flash" : "")} key={index} style={{
                                                    top:rec.top,
                                                    left:rec.left,
                                                    width:rec.width,
                                                    height:rec.height
                                                }}>
                                                    <span onClick={()=>{
                                                        if(this.state.clickedBoundingBoxIndex !== index){
                                                            this.setState({
                                                                clickedBoundingBoxIndex:index
                                                            });
                                                        } else {
                                                            this.setState({
                                                                clickedBoundingBoxIndex:-1
                                                            });
                                                        }
                                                    }}>{(this.state.selectedBoundingBoxSubcategories[rec.name + "_" + index] !== undefined ? this.state.selectedBoundingBoxSubcategories[rec.name + "_" + index] + " " + rec.name : rec.name)}</span>
                                                    {
                                                        this.state.clickedBoundingBoxIndex == index ?
                                                        <div className="rec-drop">
                                                            {
                                                                rec.subcat.length > 0 ?
                                                                <div className="subcats">
                                                                    {
                                                                        rec.subcat.map((sub,sindex)=>{
                                                                            return(
                                                                                <div 
                                                                                    className={`rec-subcat` + (this.state.selectedBoundingBoxSubcategories[rec.name + "_" + index] == sub ? " selected" : "")} 
                                                                                    key={sindex}
                                                                                    onClick={()=>{
                                                                                        var stateCopy = this.state.selectedBoundingBoxSubcategories;
                                                                                        stateCopy[rec.name + "_" + index] = sub;
                                                                                        this.setState({
                                                                                            selectedBoundingBoxSubcategories:stateCopy,
                                                                                            clickedBoundingBoxIndex:-1
                                                                                        });
                                                                                    }}>{sub}</div>
                                                                            );
                                                                        })
                                                                    }
                                                                </div>
                                                                :
                                                                <div className="norecsubcat">No Subcategories</div>
                                                            }
                                                        </div>
                                                        :
                                                        null
                                                    }
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                            <div className="top-navigation">
                                <div className={`nav-item` + (this.state.locationCardOpen ? " selected" : "")}>
                                    <div className="nmae" onClick={()=>{
                                        this.setState({
                                            locationCardOpen:true,
                                            imageQualityCardOpen:false,
                                            objectsCardOpen:false,
                                            viewTypeCardOpen:false,
                                            showBoundingBoxes:false,
                                            outsideViewCardOpen:false
                                        });
                                    }}>Location</div>
                                    <div className={`nav-card` + (this.state.locationCardOpen ? " show" : "")}>
                                        <img className="close" src={require("../images/clear-button-dark.svg")} onClick={()=>{
                                            this.setState({
                                                locationCardOpen:false
                                            })
                                        }}/>
                                        <Map center={this.state.center} zoom={13} ref={mapnode => this.map = mapnode}>
                                            <TileLayer
                                                attribution="&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
                                                url="https://api.mapbox.com/styles/v1/vsvanshi/cjhjd3dmv0g5u2ro27j81ol3o/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoidnN2YW5zaGkiLCJhIjoiY2l0YnF3Yzl1MDd0YjJvczZiempmczc0NyJ9.PUqEP-Mrl2Fxl82v6jyCOA"
                                            />
                                            <Marker 
                                                ref={node => this.centerMarker = node}
                                                position={this.state.center} 
                                                icon={MARKER_ICON} 
                                                draggable={true} 
                                                onClick={()=>{
                                                    this.getIscoChronesData({lat:this.state.center[0], lng:this.state.center[1]})
                                                }} 
                                                ondragend={()=>{
                                                    const { lat, lng } = this.centerMarker.leafletElement.getLatLng();
                                                    this.map.leafletElement.openPopup(this.mainPopup.leafletElement);
                                                    this.getIscoChronesData({lat:lat, lng:lng})
                                                    this.setState({
                                                        center:[lat,lng]
                                                    });
                                                }}>
                                                <Popup ref={node => this.mainPopup = node}>
                                                    <div className="popup-container">
                                                        {
                                                            this.state.dataToShow != null 
                                                            ?
                                                            <div className="location-info">
                                                                <div className="row-item">
                                                                    <img src={require("../images/beach.svg")} />
                                                                    <div className="lbl">Beach Nearby</div>
                                                                    <div className="val">
                                                                        {
                                                                            this.state.dataToShow.beach.exists ? this.state.dataToShow.beach.name : "No"
                                                                        }
                                                                    </div>
                                                                </div>
                                                                <div className="row-item">
                                                                    <img src={require("../images/vegetation.svg")} />
                                                                    <div className="lbl">Vegetation Score</div>
                                                                    <div className="val">
                                                                        {
                                                                            this.state.dataToShow.vegetation.score
                                                                        }
                                                                    </div>
                                                                </div>
                                                                <div className="row-item">
                                                                    <img src={require("../images/sea.svg")} />
                                                                    <div className="lbl">Ocean</div>
                                                                    <div className="val">
                                                                        {
                                                                            this.state.dataToShow.ocean !== "" ? this.state.dataToShow.ocean : "NA"
                                                                        }
                                                                    </div>
                                                                </div>
                                                                <div className="row-item">
                                                                    <img src={require("../images/river.svg")} />
                                                                    <div className="lbl">River</div>
                                                                    <div className="val">
                                                                        {
                                                                            this.state.dataToShow.river.name !== "" ? this.state.dataToShow.river.name : "NA" 
                                                                        }
                                                                        {
                                                                            this.state.dataToShow.river.distance !== "" ? " (" + (this.state.dataToShow.river.distance/1000).toFixed(2) + " kms)" : ""
                                                                        }
                                                                    </div>
                                                                </div>
                                                                <div className="row-item">
                                                                    <img src={require("../images/attractive.svg")} />
                                                                    <div className="lbl">Attractions:</div>
                                                                    {
                                                                        this.state.dataToShow.attraction.length > 0 ?
                                                                        <div className="val">
                                                                        {
                                                                            this.state.dataToShow.attraction.map((attr,index)=>{
                                                                                return(
                                                                                    <span key={index}>{attr.name}</span>
                                                                                )
                                                                            })
                                                                        }
                                                                    </div>
                                                                    :
                                                                    <div className="val">NA</div>
                                                                    }
                                                                </div>
                                                                <div className="btn" onClick={()=>{
                                                                    
                                                                }}>
                                                                    Select
                                                                </div>
                                                            </div>
                                                            :
                                                            <div className="imsg"> {this.state.apiError || "Fetching Data..."} </div>
                                                        }
                                                    </div>
                                                </Popup>
                                            </Marker>
                                        </Map>
                                    </div>
                                </div>
                                <div className="nav-item">
                                    <div className="nmae" onClick={()=>{
                                        this.setState({
                                            imageQualityCardOpen:true,
                                            locationCardOpen:false,
                                            viewTypeCardOpen:false,
                                            objectsCardOpen:false,
                                            showBoundingBoxes:false,
                                            outsideViewCardOpen:false
                                        })    
                                    }}>Image Quality</div>
                                    <div className={`nav-card` + (this.state.imageQualityCardOpen ? " show" : "")}>
                                        <img className="close" src={require("../images/clear-button-dark.svg")} onClick={()=>{
                                            this.setState({
                                                imageQualityCardOpen:false
                                            })
                                        }}/>
                                        {
                                            this.state.imageQualityCallInProgress ?
                                            <div className="imgquality-progress">Please wait...</div>
                                            :
                                            null
                                        }
                                        <div className="img-quality-wrap">
                                            {
                                                this.state.goodImageQualities.length > 0 ?
                                                <div className="quality-wrap">
                                                    <div className="quality-title">Good things about this image</div>
                                                    {
                                                        this.state.goodImageQualities.map((gq,index)=>{
                                                            return(
                                                                <div className="qitm good" key={index}>{gq}</div>
                                                            );
                                                        })
                                                    }
                                                </div>
                                                :
                                                <div className="quality-err">No Good things in this image</div>
                                            }
                                            {
                                                this.state.badImageQualities.length > 0 ?
                                                <div className="quality-wrap">
                                                    <div className="quality-title">Bad things about this image</div>
                                                    {
                                                        this.state.badImageQualities.map((bq,index)=>{
                                                            return(
                                                                <div className="qitm bad" key={index}>{bq}</div>
                                                            );
                                                        })
                                                    }
                                                </div>
                                                :
                                                <div className="quality-err">No Bad things in this image</div>
                                            }
                                            {
                                                this.state.imageScore !== null ?

                                                <div className="quality-img-score">
                                                    <div className="quality-title" style={{fontSize:"24px"}}>Aesthetic Score</div>
                                                    <div style={{display:"flex",alignItems:"center"}}>
                                                        <div className="score-lbl">{this.getScoreLabel(this.state.imageScore) + " (" + this.state.imageScore + "%)"}</div>
                                                        <div className='prg-wrap'>
                                                            <CircularProgressbar
                                                                percentage={this.state.imageScore}
                                                                styles={{
                                                                    path: { stroke: this.getImageScoreColor(this.state.imageScore) },
                                                                    text: { fill: this.getImageScoreColor(this.state.imageScore), fontSize: '22px' },
                                                                }}/>
                                                        </div>
                                                    </div>
                                                </div>
                                                :
                                                <div className="quality-err">Could not get Aesthetic score for this image</div>
                                            }
                                        </div>
                                    </div>
                                </div>

                                <div className="nav-item">
                                    <div className="nmae" onClick={()=>{
                                        this.setState({
                                            viewTypeCardOpen:true,
                                            locationCardOpen:false,
                                            objectsCardOpen:false,
                                            imageQualityCardOpen:false,
                                            showBoundingBoxes:false,
                                            outsideViewCardOpen:false
                                        })    
                                    }}>Scene Type</div>
                                    <div className={`nav-card` + (this.state.viewTypeCardOpen ? " show" : "")}>
                                        <img className="close" src={require("../images/clear-button-dark.svg")} onClick={()=>{
                                            this.setState({
                                                viewTypeCardOpen:false
                                            })
                                        }}/>
                                        {
                                            this.state.viewTypeApicallInprogress ?
                                            <div className="imgquality-progress">Please wait...</div>
                                            :
                                            null
                                        }
                                        {
                                            this.state.imageViewData != null ?
                                                <div className="img-quality-wrap">
                                                    <div className="row-item">
                                                        <div className="qlbl">Environment Type</div>
                                                        <div className="qval">
                                                        {
                                                            this.state.imageViewData.typeOfEnvironment.toUpperCase()
                                                        }
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <div className="row-item">
                                                        <div className="qlbl">Scene Categories : </div>
                                                        {
                                                            this.state.imageViewData.sceneCategories.map((sc,index)=>{
                                                                return(
                                                                    <div 
                                                                        className={`qtag` + (this.state.selectedImageCategoryIndex == index ? " selected" : "")} 
                                                                        key={index}
                                                                        onClick={()=>{
                                                                            if(this.state.selectedImageCategoryIndex !== index){
                                                                                this.setState({
                                                                                    selectedImageCategoryIndex:index
                                                                                },()=>{
                                                                                    this.getSubcategories();
                                                                                })
                                                                            } else {
                                                                                this.setState({
                                                                                    selectedImageCategoryIndex:-1
                                                                                })
                                                                            }
                                                                        }}>
                                                                        {sc[0]}
                                                                    </div>
                                                                )
                                                            })
                                                        }
                                                    </div>
                                                    {
                                                        this.state.selectedImageCategoryIndex !== -1 ? 
                                                        <div className="row-item">
                                                            <div className="qlbl">Sub Categories : </div>
                                                            <div>
                                                                {
                                                                    this.state.imageSubcategories.map((sc,index)=>{
                                                                        return(
                                                                            <div className="qtag s" key={index}>
                                                                                {sc}
                                                                            </div>    
                                                                        )
                                                                    })
                                                                }
                                                            </div>
                                                        </div>
                                                        :
                                                        null
                                                    }
                                                </div>
                                                :
                                                <div className="quality-err">Could not get Image View Data</div>
                                        }
                                    </div>
                                </div>

                                <div className="nav-item">
                                    <div className="nmae" onClick={()=>{
                                        this.setState({
                                            objectsCardOpen:true,
                                            locationCardOpen:false,
                                            viewTypeCardOpen:false,
                                            imageQualityCardOpen:false,
                                            outsideViewCardOpen:false,
                                            showBoundingBoxes:true
                                        })    
                                    }}>Objects</div>
                                    <div className={`nav-card` + (this.state.objectsCardOpen ? " show" : "")}>
                                        <img className="close" src={require("../images/clear-button-dark.svg")} onClick={()=>{
                                            this.setState({
                                                objectsCardOpen:false
                                            })
                                        }}/>  
                                        {
                                            this.state.imageObjectData !== null ?
                                            <div className="img-quality-wrap">
                                            {
                                                Object.keys(this.state.imageObjectData.tags.objects.bbox).map((box,index)=>{
                                                    return(
                                                        <div 
                                                            className="sa-tag" 
                                                            key={index}
                                                            onMouseEnter={()=>{
                                                                var stateCopy = this.state.currentRectangles;
                                                                stateCopy.map((cr)=>{
                                                                    if(cr.name == box){
                                                                        cr.flash = true
                                                                    }
                                                                });
                                                                this.setState({
                                                                    currentRectangles:stateCopy
                                                                });
                                                            }} onMouseLeave={()=>{
                                                                var stateCopy = this.state.currentRectangles;
                                                                stateCopy.map((cr)=>{
                                                                    if(cr.name == box){
                                                                        cr.flash = false
                                                                    }
                                                                });
                                                                this.setState({
                                                                    currentRectangles:stateCopy
                                                                });
                                                            }}>{box.toUpperCase()}</div>
                                                    )
                                                })
                                            }
                                            </div>
                                            :
                                            <div className="quality-err">Could not get Image Object Data</div>
                                        }
                                    </div>     
                                </div>

                                <div className="nav-item">
                                    <div className="nmae" onClick={()=>{
                                        this.setState({
                                            outsideViewCardOpen:true,
                                            imageQualityCardOpen:false,
                                            locationCardOpen:false,
                                            viewTypeCardOpen:false,
                                            objectsCardOpen:false,
                                            showBoundingBoxes:false
                                        })    
                                    }}>Outside View</div>
                                    <div className={`nav-card` + (this.state.outsideViewCardOpen ? " show" : "")}>
                                        <img className="close" src={require("../images/clear-button-dark.svg")} onClick={()=>{
                                            this.setState({
                                                outsideViewCardOpen:false
                                            })
                                        }}/>
                                        {
                                            this.state.outsideApicallinprogress ?
                                            <div className="imgquality-progress">Please wait...</div>
                                            :
                                            null
                                        }
                                        {
                                            this.state.outsideObjectData !== null ?
                                                <div className="img-quality-wrap">
                                                    <div className="ov-wrap">
                                                        {
                                                            this.state.outsideObjectData.monument !== "" ?
                                                            <div className="ov-row">
                                                                <div className="ov-lbl">Monument:</div>
                                                                <div className="ov-val">{this.state.outsideObjectData.monument}</div>
                                                            </div>
                                                            :
                                                            <div className="ov-nodata">No Monument Identified</div>
                                                        }
                                                    </div>

                                                    <div className="ov-wrap">
                                                        {
                                                            this.state.outsideObjectData["mountain/hill_view"] ?
                                                            <div className="ov-title" style={{display:"none"}}>Hill View:</div>
                                                            :
                                                            null
                                                        }
                                                        
                                                        {
                                                        this.state.outsideObjectData["mountain/hill_view"] ?
                                                        <div className="ov-row">
                                                            <div className="ov-lbl">Hill View :</div>
                                                            <div className="ov-val">Hill View Present</div>
                                                        </div>
                                                        :
                                                        null
                                                    }
                                                    </div>
                                                    
                                                    <div className="ov-wrap">
                                                    {
                                                        this.state.outsideObjectData["river/lake_view"] && this.state.outsideObjectData["river/lake_view"].river && this.state.outsideObjectData["river/lake_view"].river.name !== "" ?
                                                        <div className="ov-title" style={{display:"none"}}>River/Lake View:</div>
                                                        :
                                                        null
                                                    }
                                                        
                                                        {
                                                            this.state.outsideObjectData["river/lake_view"] && this.state.outsideObjectData["river/lake_view"].river && this.state.outsideObjectData["river/lake_view"].river.name !== "" ?
                                                            <div className="ov-row">
                                                                <div className="ov-lbl">River View :</div>   
                                                                <div className="ov-val">{this.state.outsideObjectData["river/lake_view"].river.name + " (" + (this.state.outsideObjectData["river/lake_view"].river.distance/1000).toFixed(2) + " km)"}</div>
                                                            </div>
                                                            :
                                                            null
                                                        }
                                                    </div>
                                                    <div className="ov-wrap">
                                                    {
                                                        this.state.outsideObjectData["sea/beach_view"] && this.state.outsideObjectData["sea/beach_view"].beach && this.state.outsideObjectData["sea/beach_view"].beach.name !== "" ?  
                                                        <div className="ov-title" style={{display:"none"}}>Sea/Beach View:</div>
                                                        :
                                                        null
                                                    }
                                                        
                                                        {
                                                            this.state.outsideObjectData["sea/beach_view"] && this.state.outsideObjectData["sea/beach_view"].beach && this.state.outsideObjectData["sea/beach_view"].beach.name !== "" ?
                                                            <div className="ov-row">
                                                                <div className="ov-lbl">Beach</div>   
                                                                <div className="ov-val">{this.state.outsideObjectData["sea/beach_view"].beach.name + " (" + (this.state.outsideObjectData["sea/beach_view"].beach.distance/1000).toFixed(2) + " km)"}</div>
                                                            </div>
                                                            :
                                                            null
                                                        }
                                                        {
                                                            this.state.outsideObjectData["sea/beach_view"] && this.state.outsideObjectData["sea/beach_view"].coast && this.state.outsideObjectData["sea/beach_view"].coast.distance !== "" ?
                                                            <div className="ov-row">
                                                                <div className="ov-lbl">Coast</div>   
                                                                <div className="ov-val">{" (" + (this.state.outsideObjectData["sea/beach_view"].coast.distance/1000).toFixed(2) + " km)"}</div>
                                                            </div>
                                                            :
                                                            null
                                                        }
                                                        {
                                                           this.state.outsideObjectData["sea/beach_view"] && this.state.outsideObjectData["sea/beach_view"].ocean ?
                                                            <div className="ov-row">
                                                                <div className="ov-lbl">Ocean</div>   
                                                                <div className="ov-val">{this.state.outsideObjectData["sea/beach_view"].ocean}</div>
                                                            </div>
                                                            :
                                                            null
                                                        }
                                                    </div>
                                                </div>
                                            :
                                            <div className="quality-err">Could not get Outside view Data</div>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    :
                    null
                }
           </div>
        );
    }

    getImageScoreColor(imageScore){
        if(imageScore < 20){
            return "#ff0000";
        } else if(imageScore >=20 && imageScore < 35){
            return "#f3b31d";
        } else if(imageScore >=35 && imageScore < 50){
            return "#f3b31d";
        } else if(imageScore >=50 && imageScore < 65){
            return "#1db6f3";
        } else if(imageScore >=65 && imageScore < 80){
            return "#d2ec0b";
        } else if (imageScore >=80 ){
            return "#12e624";
        }
    }

    getScoreLabel(imageScore){
        if(imageScore < 20){
            return "Trash It";
        } else if(imageScore >=20 && imageScore < 35){
            return "Hmm Poor";
        } else if(imageScore >=35 && imageScore < 50){
            return "Just OK";
        } else if(imageScore >=50 && imageScore < 65){
            return "Good One";
        } else if(imageScore >=65 && imageScore < 80){
            return "Excellent Capture";
        } else if (imageScore >=80 ){
            return "God like";
        }
    }
    getSubcategories(){
        var currentCategory = this.state.imageViewData.sceneCategories[this.state.selectedImageCategoryIndex][0];
        var categoryId = -1;
        Object.keys(this.state.imageViewData.category).map((ct)=>{
            if(this.state.imageViewData.category[ct] == currentCategory){
                categoryId = ct;
            }
        });
        var arr = [];
        if(categoryId !== -1){
            Object.keys(this.state.imageViewData.subcategory[categoryId]).map((subc)=>{
                arr.push(this.state.imageViewData.subcategory[categoryId][subc]);
            });
            this.setState({
                imageSubcategories:arr
            });
        }
    }
}